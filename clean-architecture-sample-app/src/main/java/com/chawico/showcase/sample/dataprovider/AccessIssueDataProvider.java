package com.chawico.showcase.sample.dataprovider;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.chawico.showcase.sample.entrypoint.CorrectHelloWorldController;

/**
 * Representation of a Data Provider that's not respecting Clean Architecture access definitions.
 *
 * @author Charles Coco {@literal <charleswc@gmail.com>}
 * @since 09-16-2019
 */
@Service
public class AccessIssueDataProvider {

  @Autowired
  private CorrectHelloWorldController controller;
  
  /**
   * Makes an illegal access to the Controller.
   */
  public void doSomething() {
    this.controller.sayHello();
  }
}
