package com.chawico.showcase.sample.usecase.domain;

import java.util.Date;
import lombok.Getter;
import lombok.Setter;

/**
 * Representation of a Domain object.
 *
 * @author Charles Coco {@literal <charleswc@gmail.com>}
 * @since 09/13/2019
 */
@Getter
@Setter
public class CorrectHelloWorldDomain {

  private String phrase;
  private Date date;
}
