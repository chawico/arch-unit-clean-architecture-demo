package com.chawico.showcase.sample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Responsible for the application initialization.
 *
 * @author Charles Coco {@literal <charleswc@gmail.com>}
 * @since 09/13/2019
 */
@SpringBootApplication
public class BootSampleApplication {

  /**
   * Main method
   * 
   * @param args {@code String[]} - initialization arguments.
   */
  public static void main(String... args) {
    SpringApplication.run(BootSampleApplication.class, args);
  }
}
