package com.chawico.showcase.sample.dataprovider.mapper;

import com.chawico.showcase.sample.dataprovider.entity.CorrectHelloWorldEntity;
import com.chawico.showcase.sample.usecase.domain.CorrectHelloWorldDomain;

/**
 * Converts an object retrieved from service to a domain object.
 *
 * @author Charles Coco {@literal <charleswc@gmail.com>}
 * @since 09/13/2019
 */
public class CorrectHelloWorldDataProviderMapper {

  /**
   * Private constructor in order to avoid instantiation.
   */
  private CorrectHelloWorldDataProviderMapper() {

  }

  /**
   * Converts a {@code CorrectHelloWorldEntity} to an object that represents the domain (core) of
   * the application.
   * 
   * @param helloWorldEntity {@code CorrectHelloWorldEntity} - Hello World retrieved from service.
   * @return data provider object converted to domain object.
   */
  public static final CorrectHelloWorldDomain toDomain(CorrectHelloWorldEntity helloWorldEntity) {
    CorrectHelloWorldDomain domain = new CorrectHelloWorldDomain();
    domain.setPhrase(helloWorldEntity.getPhrase());
    domain.setDate(helloWorldEntity.getDate());

    return domain;
  }
}
