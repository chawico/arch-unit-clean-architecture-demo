package com.chawico.showcase.sample.entrypoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import com.chawico.showcase.sample.dataprovider.CorrectHelloWorldDataProvider;

/**
 * Representation of a Controller that's not respecting Clean Architecture access definitions.
 *
 * @author Charles Coco {@literal <charleswc@gmail.com>}
 * @since 09-16-2019
 */
@RestController
public class AcccesIssueController {

  @Autowired
  private CorrectHelloWorldDataProvider dataProvider;
  
  /**
   * Makes an illegal access to the Data Provider.
   * 
   * @return text
   */
  @GetMapping("/wrong")
  public String wrong() {
    dataProvider.sayHello();
    return "Wrong endpoint called";
  }
}
