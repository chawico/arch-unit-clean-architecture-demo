package com.chawico.showcase.sample.usecase.gateway;

import com.chawico.showcase.sample.usecase.domain.CorrectHelloWorldDomain;

/**
 * Gateway responsible for retrieving the Hello World phrase.
 *
 * @author Charles Coco {@literal <charleswc@gmail.com>}
 * @since 09/13/2019
 */
public interface CorrectHelloWorldGateway {

  /**
   * Returns the well known Hello World phrase + date.
   * 
   * @return phrase + date.
   */
  CorrectHelloWorldDomain sayHello();
}
