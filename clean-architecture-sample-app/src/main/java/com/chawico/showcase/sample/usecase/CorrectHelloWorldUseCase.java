package com.chawico.showcase.sample.usecase;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.chawico.showcase.sample.usecase.domain.CorrectHelloWorldDomain;
import com.chawico.showcase.sample.usecase.gateway.CorrectHelloWorldGateway;

/**
 * Use Case responsible for retrieving the Hello World phrase.
 *
 * @author Charles Coco {@literal <charleswc@gmail.com>}
 * @since 09/13/2019
 */
@Component
public class CorrectHelloWorldUseCase {

  @Autowired
  private CorrectHelloWorldGateway helloWorldGateway;

  /**
   * Retrieves the well known Hello World phrase.
   * 
   * @return domain
   */
  public CorrectHelloWorldDomain sayHello() {
    return this.helloWorldGateway.sayHello();
  }
}
