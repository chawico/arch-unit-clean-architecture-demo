package com.chawico.showcase.sample.dataprovider;

import java.util.Date;
import org.springframework.stereotype.Service;
import com.chawico.showcase.sample.dataprovider.entity.CorrectHelloWorldEntity;
import com.chawico.showcase.sample.dataprovider.mapper.CorrectHelloWorldDataProviderMapper;
import com.chawico.showcase.sample.usecase.domain.CorrectHelloWorldDomain;
import com.chawico.showcase.sample.usecase.gateway.CorrectHelloWorldGateway;

/**
 * Data provider.
 *
 * @author Charles Coco {@literal <charleswc@gmail.com>}
 * @since 09/13/2019
 */
@Service
public class CorrectHelloWorldDataProvider implements CorrectHelloWorldGateway {

  @Override
  public CorrectHelloWorldDomain sayHello() {
    CorrectHelloWorldEntity entity = new CorrectHelloWorldEntity();
    entity.setPhrase("Hello world");
    entity.setDate(new Date());

    return CorrectHelloWorldDataProviderMapper.toDomain(entity);
  }
}
