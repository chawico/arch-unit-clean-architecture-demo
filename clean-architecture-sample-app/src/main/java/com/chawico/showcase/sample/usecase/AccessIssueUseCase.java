package com.chawico.showcase.sample.usecase;

import org.springframework.stereotype.Service;
import com.chawico.showcase.sample.entrypoint.CorrectHelloWorldController;

/**
 * Representation of an Use Case that's not respecting Clean Architecture access definitions.
 *
 * @author Charles Coco {@literal <charleswc@gmail.com>}
 * @since 09-16-2019
 */
@Service
public class AccessIssueUseCase {

  private CorrectHelloWorldController controller;
  
  /**
   * Makes an illegal access to the Controller.
   */
  public void doSomething() {
    this.controller.sayHello();
  }
}
