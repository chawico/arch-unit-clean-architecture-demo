package com.chawico.showcase.sample.dataprovider;

import org.springframework.web.bind.annotation.RestController;

/**
 * Representation of a Controller that's not respecting Clean Architecture access definitions.
 *
 * @author Charles Coco {@literal <charleswc@gmail.com>}
 * @since 09-16-2019
 */
@RestController
public class MisplacedEntryPoint {

}
