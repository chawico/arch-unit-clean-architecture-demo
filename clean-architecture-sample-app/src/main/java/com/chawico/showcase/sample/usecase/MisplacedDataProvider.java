package com.chawico.showcase.sample.usecase;

import org.springframework.stereotype.Service;

/**
 * Representation of misplaced Data Provider.
 *
 * @author Charles Coco {@literal <charleswc@gmail.com>}
 * @since 09-16-2019
 */
@Service
public class MisplacedDataProvider {
  
  public int doSomething(int value1, int value2) {
    return value1 + value2;
  }
}
