package com.chawico.showcase.sample.entrypoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import com.chawico.showcase.sample.entrypoint.model.mapper.CorrectHelloWorldModelMapper;
import com.chawico.showcase.sample.entrypoint.model.response.CorrectHelloWorldModelResponse;
import com.chawico.showcase.sample.usecase.CorrectHelloWorldUseCase;
import com.chawico.showcase.sample.usecase.domain.CorrectHelloWorldDomain;

/**
 * Controller responsible for retrieving Hello World
 *
 * @author Charles Coco {@literal <charleswc@gmail.com>}
 * @since 09/13/2019
 */
@RestController
public class CorrectHelloWorldController {

  @Autowired
  private CorrectHelloWorldUseCase helloWorldUseCase;

  /**
   * Retrieves Hello World phrase.
   * 
   * @return data representation of Hello World
   */
  @GetMapping(value = "/hello")
  public CorrectHelloWorldModelResponse sayHello() {

    CorrectHelloWorldDomain domain = this.helloWorldUseCase.sayHello();
    CorrectHelloWorldModelResponse response = CorrectHelloWorldModelMapper.fromDomain(domain);

    return response;
  }
}
