package com.chawico.showcase.sample.dataprovider.entity;

import java.util.Date;
import lombok.Getter;
import lombok.Setter;

/**
 * Representation of a table that describes a Person.
 *
 * @author Charles Coco {@literal <charleswc@gmail.com>}
 * @since 09/13/2019
 */
@Getter
@Setter
public class CorrectHelloWorldEntity {

  private String phrase;
  private Date date;
}
