package com.chawico.showcase.sample.entrypoint.model.mapper;

import java.util.Date;
import com.chawico.showcase.sample.entrypoint.model.response.CorrectHelloWorldModelResponse;
import com.chawico.showcase.sample.usecase.domain.CorrectHelloWorldDomain;

/**
 * Responsible for converting models between Entry Point and Use Case layers.
 *
 * @author Charles Coco {@literal <charleswc@gmail.com>}
 * @since 09/13/2019
 */
public class CorrectHelloWorldModelMapper {

  /**
   * Private constructor to avoid instantiation.
   */
  private CorrectHelloWorldModelMapper() {

  }

  /**
   * Converts a {@code PersonDomain} to an object that'll be returned as a response to the request.
   * 
   * @param helloWorldDomain {@code CorrectHelloWorldDomain} - domain.
   * @return entry point representation of domain object.
   */
  public static final CorrectHelloWorldModelResponse fromDomain(
      CorrectHelloWorldDomain helloWorldDomain) {

    CorrectHelloWorldModelResponse response = new CorrectHelloWorldModelResponse();
    response.setPhrase(helloWorldDomain.getPhrase());
    
    Date date = helloWorldDomain.getDate();
    if (date != null) {
      response.setDate(helloWorldDomain.getDate().toString());
    }

    return response;
  }
}
