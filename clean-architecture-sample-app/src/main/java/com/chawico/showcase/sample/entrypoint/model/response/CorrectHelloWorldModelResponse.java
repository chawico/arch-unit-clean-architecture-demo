package com.chawico.showcase.sample.entrypoint.model.response;

import lombok.Getter;
import lombok.Setter;

/**
 * Structure that represents Hello World data that'll be exposed to the outerworld.
 *
 * @author Charles Coco {@literal <charleswc@gmail.com>}
 * @since 09/13/2019
 */
@Getter
@Setter
public class CorrectHelloWorldModelResponse {

  private String phrase;
  private String date;
}
