package com.chawico.showcase.archunit.rule;

import static com.chawico.showcase.archunit.constants.CleanArchitectureConstants.DATA_PROVIDER_PACKAGE;
import static com.chawico.showcase.archunit.constants.CleanArchitectureConstants.ENTRY_POINT_PACKAGE;
import static com.chawico.showcase.archunit.constants.CleanArchitectureConstants.USE_CASE_PACKAGE;
import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;
import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.lang.ArchRule;

/**
 * Set of rules to validate the structure (classes x packages) according to Clean Architecture.
 *
 * @author Charles Coco {@literal <charleswc@gmail.com>}
 * @since 09/13/2019
 */
public class CleanArchitectureStructureRule {

  private static final String ENTRY_POINT_NAME_CONVENTION = "Controller";
  private static final String USE_CASE_NAME_CONVENTION = "UseCase";
  private static final String DATA_PROVIDER_NAME_CONVENTION = "DataProvider";
  
  /**
   * Validates that all classes that uses RestController or Controller annotation AND with name
   * ending in Controller resides in the entry point package.
   * 
   * @param classes {@code JavaClasses} - the classes that will be validated
   */
  public void checkEntryPointStructure(JavaClasses classes) {
    // @formatter:off
    ArchRule annotationsRule = classes()
        .that()
          .areAnnotatedWith(RestController.class).or()
          .areAnnotatedWith(Controller.class)
        .should()
          .resideInAPackage(ENTRY_POINT_PACKAGE).andShould()
          .haveSimpleNameEndingWith(ENTRY_POINT_NAME_CONVENTION);
    ArchRule packageRule = classes()
        .that()
          .haveSimpleNameEndingWith(ENTRY_POINT_NAME_CONVENTION)
        .should()
          .resideInAPackage(ENTRY_POINT_PACKAGE);
    // @formatter:on

    annotationsRule.check(classes);
    packageRule.check(classes);
  }

  /**
   * Validates that all classes that have name ending in UseCase resides in the use case package.
   * 
   * @param classes {@code JavaClasses} - the classes that will be validated
   */
  public void checkUseCaseStructure(JavaClasses classes) {
    // @formatter:off
	ArchRule packageRule = classes()
	    .that()
	      .haveSimpleNameEndingWith(USE_CASE_NAME_CONVENTION)
	    .should()
	      .resideInAPackage(USE_CASE_PACKAGE);
	// @formatter:on

    packageRule.check(classes);
  }

  /**
   * Validates that all classes that have name ending in DataProvider resides in the data provider package.
   * 
   * @param classes {@code JavaClasses} - the classes that will be validated
   */
  public void checkDataProviderStructure(JavaClasses classes) {
    // @formatter:off
    ArchRule packageRule = classes()
        .that()
          .haveSimpleNameEndingWith(DATA_PROVIDER_NAME_CONVENTION)
        .should()
          .resideInAPackage(DATA_PROVIDER_PACKAGE);
    // @formatter:on

    packageRule.check(classes);
  }
}
