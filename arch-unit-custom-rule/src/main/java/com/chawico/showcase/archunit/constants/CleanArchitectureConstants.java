package com.chawico.showcase.archunit.constants;

/**
 * Constants related to Clean Architecture.
 *
 * @author Charles Coco {@literal <charleswc@gmail.com>}
 * @since 09/13/2019
 */
public class CleanArchitectureConstants {

	public static final String ENTRY_POINT_PACKAGE = "..entrypoint..";
	public static final String USE_CASE_PACKAGE = "..usecase..";
	public static final String DATA_PROVIDER_PACKAGE = "..dataprovider..";

	/**
	 * Private constructor to avoid instantiation.
	 */
	private CleanArchitectureConstants() {
		
	}
}
