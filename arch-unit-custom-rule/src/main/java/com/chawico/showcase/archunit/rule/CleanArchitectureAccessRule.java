package com.chawico.showcase.archunit.rule;

import static com.chawico.showcase.archunit.constants.CleanArchitectureConstants.DATA_PROVIDER_PACKAGE;
import static com.chawico.showcase.archunit.constants.CleanArchitectureConstants.ENTRY_POINT_PACKAGE;
import static com.chawico.showcase.archunit.constants.CleanArchitectureConstants.USE_CASE_PACKAGE;
import static com.tngtech.archunit.library.Architectures.layeredArchitecture;
import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.lang.ArchRule;

/**
 * Set of rules to validate the the access between layers according to Clean Architecture.
 *
 * @author Charles Coco {@literal <charleswc@gmail.com>}
 * @since 09/13/2019
 */
public class CleanArchitectureAccessRule {

  private static final String ENTRY_POINT_LAYER = "EntryPoint";
  private static final String USE_CASE_LAYER = "UseCase";
  private static final String DATA_PROVIDER_LAYER = "DataProvider";
  
  /**
   * Validates that all classes are following the access restrictions defined by the Clean Architecture.
   * 
   * @param classes {@code JavaClasses} - the classes that will be validated
   */
  public void checkAccess(JavaClasses classes) {
    // @formatter:off
    ArchRule accessRule = layeredArchitecture()
        .layer(ENTRY_POINT_LAYER).definedBy(ENTRY_POINT_PACKAGE)
        .layer(USE_CASE_LAYER).definedBy(USE_CASE_PACKAGE)
        .layer(DATA_PROVIDER_LAYER).definedBy(DATA_PROVIDER_PACKAGE)
        
        .whereLayer(ENTRY_POINT_LAYER).mayNotBeAccessedByAnyLayer()
        .whereLayer(USE_CASE_LAYER).mayOnlyBeAccessedByLayers(ENTRY_POINT_LAYER, DATA_PROVIDER_LAYER)
        .whereLayer(DATA_PROVIDER_LAYER).mayOnlyBeAccessedByLayers(USE_CASE_LAYER);
    // @formatter:on

    accessRule.check(classes);
  }
}
